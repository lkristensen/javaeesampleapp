/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaexamples;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author hib
 */
@Entity
public class ICD extends IItem {

    private String musiccompany;
    private Integer numberofcds;
    private Float totalduration;
    private String gender;

    public String getMusiccompany() {
        return musiccompany;
    }

    public void setMusiccompany(String musiccompany) {
        this.musiccompany = musiccompany;
    }

    public Integer getNumberofcds() {
        return numberofcds;
    }

    public void setNumberofcds(Integer numberofcds) {
        this.numberofcds = numberofcds;
    }

    public Float getTotalduration() {
        return totalduration;
    }

    public void setTotalduration(Float totalduration) {
        this.totalduration = totalduration;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    

    
    @Override
    public String toString() {
        return "jpaexamples.ICD[ id=" + id + " ]";
    }
    
}
