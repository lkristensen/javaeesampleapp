/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaexamples;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hib
 */
@Stateless
public class BookConcurrency {

    @PersistenceContext(unitName = "SimpleEE6AppPU")
    private EntityManager em;
    
    public void RaiseByFive(Long id) {
        
        Book book = em.find(Book.class, id);
        
        // assume that price is 10 
        book.raisePrice(5);
        
    }
    // price is now 15
    
    public void RaiseByTen(Long id) {
        
        Book book = em.find(Book.class, id);
        
        // assume that price is 10 
        book.raisePrice(10);
        
    }
    // price is now 20
    
    public void raiseOptimistic(Long id) {
        
        Book book = em.find(Book.class, id);
                
        em.lock(book,LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        
        book.raisePrice(5);
    }
    
    public void raisePessimistic(Long id) {
        
        Book book = em.find(Book.class, id); 
                
        em.lock(book,LockModeType.PESSIMISTIC_FORCE_INCREMENT);
        
        book.raisePrice(5);
    }
}
