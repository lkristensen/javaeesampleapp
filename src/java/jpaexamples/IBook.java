/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaexamples;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author hib
 */
@Entity
public class IBook extends IItem {

    
    private String isbn;
    private String publisher;
    private Integer noofpages;
    private Boolean illustrations;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Integer getNoofpages() {
        return noofpages;
    }

    public void setNoofpages(Integer noofpages) {
        this.noofpages = noofpages;
    }

    public Boolean getIllustrations() {
        return illustrations;
    }

    public void setIllustrations(Boolean illustrations) {
        this.illustrations = illustrations;
    }
    
    
    @Override
    public String toString() {
        return "jpaexamples.IBook[ id=" + id + " ]";
    }
    
}
