/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaexamples;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author hib
 */

@Stateless
public class JPAQueries {

    @PersistenceContext(unitName = "SimpleEE6AppPU")
    private EntityManager em;
    
    // Examples of Dynamic Queries
    public List<Customer11U> findNemos() {
        
        Query query = em.createQuery("SELECT c FROM Customer11U c WHERE c.firstname = 'Nemo'");
        
        List<Customer11U> customers = query.getResultList();
        
        return customers;
    }
    
    public List<Customer11U> findByFirstNameNamed(String firstname) {
        
        Query query = em.createQuery("SELECT c FROM Customer11U c WHERE c.firstname = :custfirstname");
        
        List<Customer11U> customers = query.setParameter("custfirstname",firstname).getResultList();
                
        return customers;
    }
    
    public List<Customer11U> findByFirstNamePositional(String firstname) {
        
        return em.createQuery(
                    "SELECT c FROM Customer11U c WHERE c.firstname = ?1")
                .setParameter(1,firstname)
                .getResultList();
    }
    
    public List<Customer11U> findByNameRestrict(boolean restrict,String firstname) {
    
        String jpqlQuery = "SELECT c FROM Customer11U c";
        if (restrict) {
            jpqlQuery += " WHERE c.firstName = '" + firstname + "'";
        }
        
        Query query = em.createQuery(jpqlQuery);
        List<Customer11U> customers = query.getResultList();
        
        return customers;
    }
    
    // Example of static queries
    
    
    public List<Customer11U> findByFirstNamed(String firstname) {
        
        Query query = em.createNamedQuery("findAllCustomersWithName")
                .setParameter("custfirstname",firstname)
                .setMaxResults(10);
        
        return query.getResultList();
    }
    
    // example of native query
    public List<Customer11U> getAllCustomers() {
        
        Query query = em.createNativeQuery(
                    "SELECT * FROM customer11u", Customer11U.class);
        List<Customer11U> customers = query.getResultList();

        return customers;
    }
    
    // examples using the criteria API
    
    
    public List<Customer11U> getCustomersWithNameCriteriaWeak (String firstname) {
        
        // create a criteria
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Customer11U> criteriaQuery = builder.createQuery(Customer11U.class);
        
        // construct the query
        Root<Customer11U> c = criteriaQuery.from(Customer11U.class);
        criteriaQuery.select(c).where(builder.equal(c.get("firstname"), firstname));
        
        // execute query and get result
        TypedQuery<Customer11U> query = em.createQuery(criteriaQuery);
        List<Customer11U> customers = query.getResultList();

        return customers;
    }
    
    public List<Customer11U> getOldCustomersStrong (String firstname) {
        
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Customer11U> criteriaQuery = builder.createQuery(Customer11U.class);
        
        Root<Customer11U> c = criteriaQuery.from(Customer11U.class);
        
        criteriaQuery.select(c).where(builder.greaterThan(c.get(Customer11U_.age), 40));
        TypedQuery<Customer11U> query = em.createQuery(criteriaQuery);
        
        List<Customer11U> customers = query.getResultList();
        
        return customers;
    }
    
    public List<Customer11U> getOldCustomersStrongN (String firstname) {
        
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Customer11U> criteriaQuery = builder.createQuery(Customer11U.class);
                
        // from Customer c - do this first to get the indentification variable
        Root<Customer11U> c = criteriaQuery.from(Customer11U.class);
        
        //SELECT c FROM Customer c WHERE age > 40
        // FROM Customer c SELECT c WHERE age > 40
        criteriaQuery.select(c)
                     .where(builder.greaterThan(c.get(Customer11U_.age), 40));
       
         // embed the criteria in a query
        TypedQuery<Customer11U> query = em.createQuery(criteriaQuery);
        
        // execute query and get the result
        List<Customer11U> customers = query.getResultList();
        
        return customers;
    }
    
}
