/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaexamples;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author hib
 */
public class CustomerAM {
    
    public void StoreCustomer(Customer11U customer, Address11U address) {
        
        EntityManagerFactory emf = 
                    Persistence.createEntityManagerFactory("SimpleEE6AppPU");
        EntityManager em = emf.createEntityManager();
        
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        
        em.persist(customer);
        em.persist(address);
        
        tx.commit();
        
        em.close();
        emf.close();
    }
}
