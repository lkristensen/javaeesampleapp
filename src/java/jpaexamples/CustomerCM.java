/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpaexamples;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hib
 */
@Stateless
public class CustomerCM {

    @PersistenceContext(unitName = "SimpleEE6AppPU")
    private EntityManager em;

    public void StoreCustomer(Customer11U customer, Address11U address) {

        em.persist(customer);
        em.persist(address);
    }

    public Customer11U getCustomer(Long id) {

        Customer11U cust = em.find(Customer11U.class, id);

        return cust;

    }

    public boolean isCustomer(Long id) {

        boolean found = true;

        try {
            Customer11U cust = em.getReference(Customer11U.class, id);
        } catch (EntityNotFoundException ex) {
            found = false;
        }

        return found;
    }

    public void RemoveCustomer(Customer11U cust) {

        em.remove(cust);
        em.remove(cust.getAddress());
    }

    public void NewCustomers(Customer11U[] customers) {
        for (Customer11U customer : customers) {
            em.persist(customer);
            em.persist(customer.getAddress());
            em.flush();
        }
    }

    boolean checkEmail(Customer11U cust) {
        return false;
    }

    public String changeEmail(Long id, String email) {
        Customer11U customer = em.find(Customer11U.class, id);

        customer.setEmail(email);

        if (!checkEmail(customer)) {
            em.refresh(customer); // use the one in the database
        }

        return customer.getEmail();
    }
    
    public void UpdateCustomerName(Customer11U cust,String firstname) {
        // customer may come from the web-tier and hence not
        // in the current persistent context
        cust.setFirstname(firstname);
        
        // move the customer into the persistence context
        // so that the change can be reflected on commit
        em.merge(cust);
        
        
    }
}
