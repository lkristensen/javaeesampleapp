/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import jpaexamples.*;
/**
 *
 * @author hib
 */
@Singleton
@Startup
public class LoadAddressData {

    @PersistenceContext(unitName = "SimpleEE6AppPU")
    private EntityManager em;
    
    @PostConstruct
    public void createData() {
    
        Address11U address = new Address11U("street","city","country");
        
        Customer11U customer = new Customer11U("first","last",18,"xx@yy.com",address);
        
        em.persist(customer);
    }
}